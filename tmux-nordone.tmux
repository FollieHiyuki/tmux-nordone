#!/usr/bin/env bash

# $1: option
# $2: default value
tmux_get() {
    local value="$(tmux show -gqv "$1")"
    [ -n "$value" ] && echo "$value" || echo "$2"
}

# $1: option
# $2: value
tmux_set() {
    tmux set-option -gq "$1" "$2"
}

# Options
right_arrow_icon=$(tmux_get '@tmux_nordone_right_arrow_icon' '')
left_arrow_icon=$(tmux_get '@tmux_nordone_left_arrow_icon' '')
session_icon="$(tmux_get '@tmux_nordone_session_icon' '')"
user_icon="$(tmux_get '@tmux_nordone_user_icon' '')"
time_icon="$(tmux_get '@tmux_nordone_time_icon' '')"
date_icon="$(tmux_get '@tmux_nordone_date_icon' '')"
prefix_highlight_pos="$(tmux_get @tmux_nordone_prefix_highlight_pos 'R')"
time_format="$(tmux_get @tmux_nordone_time_format '%T')"
date_format="$(tmux_get @tmux_nordone_date_format '%F')"
# Theme option: 'nord' or 'onedark'
TC="$(tmux_get '@tmux_nordone_theme' 'nord')"
case $TC in
    'nord')
        TC='#81a1c1'
        G01='#2e3440'
        G02='#3b4252'
        G03='#4c566a'
        G04='#d8dee9'
        G05='#eceff4'

        FG="$G04"
        BG="$G02"
        ;;
    'onedark')
        TC='#61afef'
        G01='#282c34'
        G02='#3e4452'
        G03='#5c6470'
        G04='#abb2bf'
        G05='#dfdfdf'

        FG="$G04"
        BG="$G02"
        ;;
esac

# Status options
tmux_set status-interval 1
tmux_set status on

# Basic status bar colors
tmux_set status-fg "$FG"
tmux_set status-bg "$BG"
tmux_set status-attr none

# tmux-prefix-highlight
tmux_set @prefix_highlight_fg "$BG"
tmux_set @prefix_highlight_bg "$FG"
tmux_set @prefix_highlight_show_copy_mode 'on'
tmux_set @prefix_highlight_copy_mode_attr "fg=$TC,bg=$BG,bold"
tmux_set @prefix_highlight_output_prefix "#[fg=$TC]#[bg=$BG]$left_arrow_icon#[bg=$TC]#[fg=$BG]"
tmux_set @prefix_highlight_output_suffix "#[fg=$TC]#[bg=$BG]$right_arrow_icon"

# Left side of status bar
tmux_set status-left-bg "$BG"
tmux_set status-left-fg "$FG"
tmux_set status-left-length 150
user="$(whoami)"
LS="#[fg=$G01,bg=$TC,bold] $user_icon $user@#h #[fg=$TC,bg=$BG,nobold]$right_arrow_icon#[fg=$BG,bg=$G03]$right_arrow_icon#[fg=$FG,bg=$G03] $session_icon  #S #[fg=$G03,bg=$BG]$right_arrow_icon"
if [[ $prefix_highlight_pos == 'L' || $prefix_highlight_pos == 'LR' ]]; then
    LS="$LS#{prefix_highlight}"
fi
tmux_set status-left "$LS"

# Right side of status bar
tmux_set status-right-bg "$BG"
tmux_set status-right-fg "$FG"
tmux_set status-right-length 150
RS="#[fg=$G03,bg=$BG]$left_arrow_icon#[fg=$FG,bg=$G03] $time_icon  $time_format #[fg=$BG,bg=$G03]$left_arrow_icon#[fg=$TC,bg=$BG]$left_arrow_icon#[fg=$G01,bg=$TC] $date_icon  $date_format "
if [[ $prefix_highlight_pos == 'R' || $prefix_highlight_pos == 'LR' ]]; then
    RS="#{prefix_highlight}$RS"
fi
tmux_set status-right "$RS"

# Window status
tmux_set window-status-format "#[fg=$BG,bg=$G03]$right_arrow_icon#[fg=$G05,bg=$G03] #I:#W#F #[fg=$G03,bg=$BG]$right_arrow_icon"
tmux_set window-status-current-format "#[fg=$BG,bg=$TC]$right_arrow_icon#[fg=$G01,bg=$TC,bold] #I:#W#F #[fg=$TC,bg=$BG,nobold]$right_arrow_icon"

# Window separator
tmux_set window-status-separator ""

# Window status alignment
tmux_set status-justify centre

# Current window status
tmux_set window-status-current-status "fg=$TC,bg=$BG"

# Pane border
tmux_set pane-border-style "fg=$G03,bg=default"

# Active pane border
tmux_set pane-active-border-style "fg=$TC,bg=default"

# Pane number indicator
tmux_set display-panes-colour "$G03"
tmux_set display-panes-active-colour "$TC"

# Clock mode
tmux_set clock-mode-colour "$TC"
tmux_set clock-mode-style 24

# Message
tmux_set message-style "fg=$TC,bg=$BG"

# Command message
tmux_set message-command-style "fg=$TC,bg=$BG"

# Copy mode highlight
tmux_set mode-style "bg=$TC,fg=$G01"
