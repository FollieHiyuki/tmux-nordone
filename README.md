# tmux-nordone

A refactor fork of [tmux-power](https://github.com/wfxr/tmux-power), focusing on Nord and OneDark colorschemes.

# License

MIT
